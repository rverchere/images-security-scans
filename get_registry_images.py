#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# vim: expandtab sw=4 ts=4:

import json
import requests
import os

url = os.environ["GITLAB_API_URL"]
headers = {
    "PRIVATE-TOKEN": os.environ["GITLAB_PRIVATE_TOKEN"]
}

projects_api_responses = requests.get("{}/api/v4/projects/?{}".format(url,"simple=true"), headers=headers)
projects_api_responses_headers = projects_api_responses.headers
total_pages = projects_api_responses_headers["X-Total-Pages"]

for page in range (0, int(total_pages)):
    projects_api_responses = requests.get("{}/api/v4/projects/?{}{}".format(url,"simple=true&page=",page), headers=headers)
    for project in projects_api_responses.json():
        registry_api_registry_url = "{}/api/v4/projects/{}/registry/repositories?tags=true".format(url,project['id'])
        registry_api_responses = requests.get(registry_api_registry_url, headers=headers).json()
        for registry in registry_api_responses:
            if 'tags' in registry:
                for tag in registry['tags']:
                    print(tag['location'])
